spice-gtk
###############################################################################
:slug: spice-gtk 
:submenu: documentation
:modified: 2015-11-03 12:00

|frame|

.. |frame| raw:: html

    <script type="text/javascript">
        function getDocHeight(doc)
        {
            doc = doc || document;
            // stackoverflow.com/questions/1145850/
            var body = doc.body, html = doc.documentElement;
            var height = Math.max(body.scrollHeight,
                                  body.offsetHeight,
                                  html.clientHeight,
                                  html.scrollHeight,
                                  html.offsetHeight);
            return height;
        }

        function setIframeHeight(id)
        {
            var ifrm = document.getElementById(id);
            var doc = ifrm.contentDocument ?
                ifrm.contentDocument : ifrm.contentWindow.document;
            var height = getDocHeight( doc ) + 4 + "px";

            // It seems that on firefox, ie and chrome, onload behaves
            // differently - inner links might not work on firefox. The
            // workaround bellow does not work on firefox and it is not
            // necessary to chrome.
            // if (ifrm.style.height == height)
            //    return;

            ifrm.style.visibility = 'hidden';
            ifrm.style.height = "10px"; // reset to minimal height ...
            // IE opt. for bing/msn needs a bit added or scrollbar appears
            ifrm.style.height = getDocHeight( doc ) + 4 + "px";
            ifrm.style.visibility = 'visible';
        }
    </script>
    <iframe id="ifrm"
            width="100%"
            src="api/spice-gtk/"
            onload="setIframeHeight(this.id)">
    </iframe>
